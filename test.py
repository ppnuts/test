import requests
import time

n = 400
start = time.time()
markdown = "*a **a " * n  + " a** a*" * n
payload = {"text": markdown}
response = requests.post("https://gitlab.com/api/v4/markdown", data=payload) # gitlab
print(response.text)
print(time.time() - start)
